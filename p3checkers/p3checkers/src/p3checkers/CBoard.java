/**
 * 
 */

package p3checkers;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 * @author Michael Reveles
 *
 */
/**
 * @author quick
 *
 */
public class CBoard {
	private type[] spaces;
	private boolean playerColor; // false = red, true = black
	private boolean myJump;
	private boolean canMove;

	public enum type {
		RED, BLACK, REDK, BLACKK, EMPTY
	};
//	enum moveType {
//		MOVEUPLEFT, MOVEUPRIGHT, MOVEDOWNLEFT, MOVEDOWNRIGHT, JUMPUPLEFT, JUMPUPRIGHT, JUMPDOWNLEFT, JUMPDOWNRIGHT
//	};

	CBoard(type color) {
		spaces = new type[64];
		for (int i = 0; i < 64; i++) {
			spaces[i] = type.EMPTY;
		}
		for (int i = 0; i < 12; i++) {
			if ((i / 4) % 2 == 0) {
				spaces[2 * i] = color;
			} else {
				spaces[2 * i + 1] = color;
			}

		}
		type oppColor;
		if (color == type.RED) {
			oppColor = type.BLACK;
			playerColor = false;
		} else {
			oppColor = type.RED;
			playerColor = true;
		}
		for (int i = 0; i < 12; i++) {
			if ((i / 4) % 2 == 1) {
				spaces[62 - 2 * i] = oppColor;
			} else {
				spaces[63 - 2 * i] = oppColor;
			}
		}
		myJump = false;
		canMove = true;
	}
	
	public boolean getCanMove() {
		return canMove;
	}

	public void basicMove(int from, int destination) {
		spaces[destination] = spaces[from];
		spaces[from] = type.EMPTY;
		if ((destination / 8 == 7) || (destination / 8 == 0)) {// promotion check
			if (spaces[destination] == type.BLACK) {
				spaces[destination] = type.BLACKK;
			} else if (spaces[destination] == type.RED) {
				spaces[destination] = type.REDK;
			}
		}
		updateJumps();
	}

	/**
	 * @param from
	 * @param destination
	 * @param over
	 * @return true if moved piece can still jump, false otherwise
	 */
	public boolean jumpMove(int from, int destination, int over) {
		spaces[destination] = spaces[from];
		spaces[from] = type.EMPTY;
		spaces[over] = type.EMPTY;
		if ((destination / 8 == 7) || (destination / 8 == 0)) {// promotion check
			if (spaces[destination] == type.BLACK) {
				spaces[destination] = type.BLACKK;
			} else if (spaces[destination] == type.RED) {
				spaces[destination] = type.REDK;
			}
		}
		boolean out;
		if (spotHasJump(destination)) {
			return true;
		} else {
			updateJumps();
			return false;
		}
	}

	private boolean spotHasJump(int location) {
		boolean jumpUp = true, jumpDown = true, jumpLeft = true, jumpRight = true, playerPiece = false;
		type pColor = spaces[location];
		if (playerColor) {
			if ((pColor == type.BLACK) || (pColor == type.BLACKK)) {
				playerPiece = true;
			}
		} else {
			if ((pColor == type.RED) || (pColor == type.REDK)) {
				playerPiece = true;
			}
		}
		if (location % 8 < 2) {
			jumpLeft = false;
		}
		if (location % 8 > 5) {
			jumpRight = false;
		}
		if (location / 8 < 2) {
			jumpDown = false;
		}
		if (location / 8 > 5) {
			jumpUp = false;
		}
		if ((spaces[location] == type.RED) || (spaces[location] == type.BLACK)) {
			if (playerPiece) {
				jumpDown = false;
			} else {
				jumpUp = false;
			}
		}
		if (jumpUp && jumpLeft) {
			if (spaces[location + 14] == type.EMPTY) {
				if (opponentPiece(location + 7)) {
					return true;
				}
			}
		}
		if (jumpUp && jumpRight) {
			if (spaces[location + 18] == type.EMPTY) {
				if (opponentPiece(location + 9)) {
					return true;
				}
			}
		}
		if (jumpDown && jumpLeft) {
			if (spaces[location - 18] == type.EMPTY) {
				if (opponentPiece(location - 9)) {
					return true;
				}
			}
		}
		if (jumpDown && jumpRight) {
			if (spaces[location - 14] == type.EMPTY) {
				if (opponentPiece(location - 7)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean[] moveList(int location, boolean[] possibleMoves) {
		for (int i = 0; i < 8; i++) {
			possibleMoves[i] = false;
		}

		if (playerColor) {// piece matches player color
			if ((spaces[location] != type.BLACK) && (spaces[location] != type.BLACKK)) {
				return possibleMoves;
			}
		} else {
			if ((spaces[location] != type.RED) && (spaces[location] != type.REDK)) {
				return possibleMoves;
			}
		}
		boolean moveLeft = true, moveRight = true, moveUp = true, moveDown = true, jumpUp = true, jumpDown = true,
				jumpLeft = true, jumpRight = true;
		if (location % 8 == 0) {
			moveLeft = false;
		}
		if (location % 8 == 7) {
			moveRight = false;
		}
		if (location / 8 == 0) {
			moveDown = false;
		}
		if (location / 8 == 7) {
			moveUp = false;
		}
		if (location % 8 < 2) {
			jumpLeft = false;
		}
		if (location % 8 > 5) {
			jumpRight = false;
		}
		if (location / 8 < 2) {
			jumpDown = false;
		}
		if (location / 8 > 5) {
			jumpUp = false;
		}
		if (myJump) {
			moveLeft = false;
			moveRight = false;
			moveUp = false;
			moveDown = false;
		}
		if ((spaces[location] == type.RED) || (spaces[location] == type.BLACK)) {
			moveDown = false;
			jumpDown = false;
		}

		if (moveUp && moveLeft) {
			if (spaces[location + 7] == type.EMPTY) {
				possibleMoves[0] = true;
			}
		}
		if (moveUp && moveRight) {
			if (spaces[location + 9] == type.EMPTY) {
				possibleMoves[1] = true;
			}
		}
		if (moveDown && moveLeft) {
			if (spaces[location - 9] == type.EMPTY) {
				possibleMoves[2] = true;
			}
		}
		if (moveDown && moveRight) {
			if (spaces[location - 7] == type.EMPTY) {
				possibleMoves[3] = true;
			}
		}
		if (jumpUp && jumpLeft) {
			if (spaces[location + 14] == type.EMPTY) {
				if (opponentPiece(location + 7)) {
					possibleMoves[4] = true;
				}
			}
		}
		if (jumpUp && jumpRight) {
			if (spaces[location + 18] == type.EMPTY) {
				if (opponentPiece(location + 9)) {
					possibleMoves[5] = true;
				}
			}
		}
		if (jumpDown && jumpLeft) {
			if (spaces[location - 18] == type.EMPTY) {
				if (opponentPiece(location - 9)) {
					possibleMoves[6] = true;
				}
			}
		}
		if (jumpDown && jumpRight) {
			if (spaces[location - 14] == type.EMPTY) {
				if (opponentPiece(location - 7)) {
					possibleMoves[7] = true;
				}
			}
		}
		return possibleMoves;
	}

	private void updateJumps() {
		myJump = false;
		canMove = false;

		boolean flag = true;
		for (int i = 0; flag && (i < 64); i += 2) {
			type pColor = spaces[i];
			if (playerColor) {
				if ((pColor == type.BLACK) || (pColor == type.BLACKK)) {
					if (spotHasJump(i)) {
						flag = false;
						myJump = true;
						canMove = true;
					}
				}
			} else {
				if ((pColor == type.RED) || (pColor == type.REDK)) {
					if (spotHasJump(i)) {
						flag = false;
						myJump = true;
						canMove = true;
					}
				}
			}
		}
		flag = false;
		if (!canMove) {
			for (int i = 0; flag && (i < 64); i += 2) {
				type pColor = spaces[i];
				if (playerColor) {
					if ((pColor == type.BLACK) || (pColor == type.BLACKK)) {
						if (spotCanMove(i)) {
							flag = false;
							canMove = true;
						}
					}
				} else {
					if ((pColor == type.RED) || (pColor == type.REDK)) {
						if (spotCanMove(i)) {
							flag = false;
							canMove = true;
						}
					}
				}
			}
		}
	}

	private boolean spotCanMove(int location) {
		boolean moveLeft = true, moveRight = true, moveUp = true, moveDown = true, jumpUp = true, jumpDown = true,
				jumpLeft = true, jumpRight = true;
		boolean playerPiece = true;
		type pColor = spaces[location];
		if (playerColor) {
			if ((pColor == type.BLACK) || (pColor == type.BLACKK)) {
				playerPiece = true;
			} else {
				return false;
			}
		} else {
			if ((pColor == type.RED) || (pColor == type.REDK)) {
				playerPiece = true;
			} else {
				return false;
			}
		}
		if (location % 8 == 0) {
			moveLeft = false;
		}
		if (location % 8 == 7) {
			moveRight = false;
		}
		if (location / 8 == 0) {
			moveDown = false;
		}
		if (location / 8 == 7) {
			moveUp = false;
		}
		if ((spaces[location] == type.RED) || (spaces[location] == type.BLACK)) {
			moveDown = false;
		}
		if (moveUp && moveLeft) {
			if (spaces[location + 7] == type.EMPTY) {
				return true;
			}
		}
		if (moveUp && moveRight) {
			if (spaces[location + 9] == type.EMPTY) {
				return true;
			}
		}
		if (moveDown && moveLeft) {
			if (spaces[location - 9] == type.EMPTY) {
				return true;
			}
		}
		if (moveDown && moveRight) {
			if (spaces[location - 7] == type.EMPTY) {
				return true;
			}
		}

		return false;
	}

	private boolean opponentPiece(int location) {
		boolean out = false;
		if (playerColor) {// piece matches player color
			if ((spaces[location] == type.RED) || (spaces[location] == type.REDK)) {
				out = true;
			}
		} else {
			if ((spaces[location] == type.BLACK) || (spaces[location] == type.BLACKK)) {
				out = true;
			}
		}
		return out;
	}

	public void render(Graphics g, CGame game) {
		SpriteSheet ss = game.getSpriteSheet();
		BufferedImage red = ss.grabImage(1, 1, 32, 32);
		BufferedImage black = ss.grabImage(1, 2, 32, 32);
		BufferedImage redking = ss.grabImage(2, 1, 32, 32);
		BufferedImage blackking = ss.grabImage(2, 2, 32, 32);
		int xoffset = 18;
		int yoffset = 243;
		for (int i = 0; i < 64; i++) {
			if (i == 8) {
				yoffset++;
			} else if (i == 32) {
				yoffset++;
			} else if (i == 56) {
				yoffset++;
			}
			if (spaces[i] == type.RED) {
				g.drawImage(red, ((i % 8) * 32 + xoffset) * CGame.SCALE, (yoffset - (i / 8) * 32) * CGame.SCALE,
						32 * CGame.SCALE, 32 * CGame.SCALE, game);
			} else if (spaces[i] == type.BLACK) {
				g.drawImage(black, ((i % 8) * 32 + xoffset) * CGame.SCALE, (yoffset - (i / 8) * 32) * CGame.SCALE,
						32 * CGame.SCALE, 32 * CGame.SCALE, game);
			} else if (spaces[i] == type.REDK) {
				g.drawImage(redking, ((i % 8) * 32 + xoffset) * CGame.SCALE, (yoffset - (i / 8) * 32) * CGame.SCALE,
						32 * CGame.SCALE, 32 * CGame.SCALE, game);
			} else if (spaces[i] == type.BLACKK) {
				g.drawImage(blackking, ((i % 8) * 32 + xoffset) * CGame.SCALE, (yoffset - (i / 8) * 32) * CGame.SCALE,
						32 * CGame.SCALE, 32 * CGame.SCALE, game);
			}

		}
	}
}
