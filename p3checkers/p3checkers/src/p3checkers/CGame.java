package p3checkers;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JFrame;

public class CGame extends Canvas implements Runnable {
	public enum STATE {
		PIECE, DIRECTION, ACCEPT, WAITING, EXTRA, CLOSE
	}

	public static final int WIDTH = 400;
	public static final int HEIGHT = WIDTH / 12 * 9;
	public static final int SCALE = 2;
	public final String TITLE = "Checkers";

	private boolean running = false;
	private Thread thread;
	private boolean host;
	
	private PrintWriter out = null;
	private BufferedReader in = null;

	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	private BufferedImage spriteSheet = null, dirSpriteSheet = null;
	private BufferedImage background = null;
	private BufferedImage accImage = null, declImage = null;
	private BufferedImage selector = null, laSelector = null;
	private SpriteSheet ss;
	private SpriteSheet dir;
	private Socket partner;
	private ServerSocket hostSocket = null;
	private CBoard board;
	private STATE currentState;
	private int space = 0;
	private int direction = 0;
	private int accept = 1;
	private boolean[] moveList = new boolean[8];

	public void init() {
		try {
		out = new PrintWriter(partner.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(partner.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		BufferedImageLoader loader = new BufferedImageLoader();
		try {
			spriteSheet = loader.loadImage("/sprite_sheet.png");
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			dirSpriteSheet = loader.loadImage("/direction.png");
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			background = loader.loadImage("/background.png");
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			accImage = loader.loadImage("/accept.png");
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			declImage = loader.loadImage("/cancel.png");
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			selector = loader.loadImage("/selector.png");
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			laSelector = loader.loadImage("/directionselector.png");
		} catch (IOException e) {
			e.printStackTrace();
		}

		ss = new SpriteSheet(spriteSheet);
		dir = new SpriteSheet(dirSpriteSheet);

		if (host) {
			board = new CBoard(CBoard.type.RED);
			currentState = STATE.PIECE;
		} else {
			board = new CBoard(CBoard.type.BLACK);
			currentState = STATE.WAITING;
		}
		for (int i = 0; i < 8; i++) {
			moveList[i] = false;
		}
		addKeyListener(new KeyInput(this));
	}

	private synchronized void start() {
		if (running)
			return;

		running = true;
		thread = new Thread(this);
		thread.start();
	}

	private synchronized void stop() {
		if (!running)
			return;

		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.exit(1);
	}

	public void run() {
		init();
		long lastTime = System.nanoTime();
		final double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		int updates = 0;
		int frames = 0;
		long timer = System.currentTimeMillis();

		while (running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			if (delta >= 1) {
				if(currentState == STATE.CLOSE) break;
				tick();
				updates++;
				delta--;

			}
			render();
			frames++;

			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				System.out.println(updates + " Ticks, Fps " + frames);
				updates = 0;
				frames = 0;
			}
		}
		try {
			partner.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			hostSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		stop();
	}

	private void tick() {
		if(currentState == STATE.PIECE) {
			if (!board.getCanMove()) {
				currentState = STATE.CLOSE;
			}
		}
		if(currentState == STATE.WAITING) {
			String message = "", split[];
			try {
				message = in.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			split = message.split(".");
			int from = 0, destination = 0, over = 66;
			if(split.length == 3) {
				from = Integer.parseInt(split[0]);
				destination = Integer.parseInt(split[1]);
				over = Integer.parseInt(split[2]);
				if(over == 66) {
					board.basicMove(63 - Integer.parseInt(split[0]), 63 - Integer.parseInt(split[1]));
				} else {
					boolean test = board.jumpMove(from, destination, over);
					if(!test) {
						currentState = STATE.PIECE;
					}
				}
			}
		}
	}

	private void render() {

		BufferStrategy bs = this.getBufferStrategy();

		if (bs == null) {
			createBufferStrategy(3);
			return;
		}

		Graphics g = bs.getDrawGraphics();

		g.drawImage(background, 0, 0, getWidth(), getHeight(), this);
		board.render(g, this);
		if (currentState != STATE.WAITING) {
			sselectorRender(g);
			dirRender(g);
		}
		if ((currentState == STATE.DIRECTION) || (currentState == STATE.EXTRA)) {
			selectorRender(g);
		}
		if (currentState == STATE.ACCEPT) {
			selectorRender(g);
			accRender(g);
		}
		g.dispose();
		bs.show();
	}

	private void sselectorRender(Graphics g) {
		int xoffset = 18;
		int yoffset = 243;
		if (space > 55) {
			yoffset = 246;
		} else if (space > 31) {
			yoffset = 245;
		} else if (space > 7) {
			yoffset = 244;
		}
		g.drawImage(selector, ((space % 8) * 32 + xoffset) * CGame.SCALE, (yoffset - (space / 8) * 32) * CGame.SCALE,
				32 * CGame.SCALE, 32 * CGame.SCALE, this);
	}

	private void selectorRender(Graphics g) {
		switch (direction) {
		case 0:
			g.drawImage(laSelector, 600, 200, 64, 64, this);
			break;
		case 1:
			g.drawImage(laSelector, 700, 200, 64, 64, this);
			break;
		case 2:
			g.drawImage(laSelector, 600, 300, 64, 64, this);
			break;
		case 3:
			g.drawImage(laSelector, 700, 300, 64, 64, this);
			break;
		}
	}

	private void accRender(Graphics g) {
		g.drawImage(declImage, 600, 400, 64, 64, this);
		g.drawImage(accImage, 700, 400, 64, 64, this);
		g.drawImage(laSelector, 600 + 100 * accept, 400, 64, 64, this);
	}

	private void dirRender(Graphics g) {
		if (moveList[0] || moveList[4]) {
			g.drawImage(dir.grabImage(1, 1, 64, 64), 600, 200, 64, 64, this);
		}
		if (moveList[1] || moveList[5]) {
			g.drawImage(dir.grabImage(3, 1, 64, 64), 700, 200, 64, 64, this);
		}
		if (moveList[2] || moveList[6]) {
			g.drawImage(dir.grabImage(1, 3, 64, 64), 600, 300, 64, 64, this);
		}
		if (moveList[3] || moveList[7]) {
			g.drawImage(dir.grabImage(3, 3, 64, 64), 700, 300, 64, 64, this);
		}
	}

	public void setHost(boolean host) {
		this.host = host;
	}

	public void setSocket(Socket socket) {
		this.partner = socket;
	}

	public void setHostSocket(ServerSocket socket) {
		this.hostSocket = socket;
	}

	public SpriteSheet getSpriteSheet() {
		return ss;
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		switch (currentState) {
		case PIECE:

			if (key == KeyEvent.VK_RIGHT) {
				if (space % 8 != 7) {
					space++;
					moveList = board.moveList(space, moveList);
				}
			} else if (key == KeyEvent.VK_LEFT) {
				if (space % 8 != 0) {
					space--;
					moveList = board.moveList(space, moveList);
				}
			} else if (key == KeyEvent.VK_UP) {
				if (space / 8 != 7) {
					space += 8;
					moveList = board.moveList(space, moveList);
				}
			} else if (key == KeyEvent.VK_DOWN) {
				if (space / 8 != 0) {
					space -= 8;
					moveList = board.moveList(space, moveList);
				}
			} else if (key == KeyEvent.VK_ENTER) {
				boolean test = false;
				for (int i = 0; i < 8; i++) {
					if (moveList[i]) {
						test = true;
					}
				}
				if (test) {
					currentState = STATE.DIRECTION;
					if (moveList[0] || moveList[4]) {
						direction = 0;
					} else if (moveList[1] || moveList[5]) {
						direction = 1;
					} else if (moveList[2] || moveList[6]) {
						direction = 2;
					} else if (moveList[3] || moveList[7]) {
						direction = 3;
					}
				}
			}
			break;
		case DIRECTION:

			if (key == KeyEvent.VK_RIGHT) {
				do {
					direction = (direction + 1) % 4;
				} while (!moveList[direction] && !moveList[direction + 4]);

			} else if (key == KeyEvent.VK_LEFT) {
				do {
					direction = (direction + 3) % 4;
				} while (!moveList[direction] && !moveList[direction + 4]);
			} else if (key == KeyEvent.VK_ENTER) {
				currentState = STATE.ACCEPT;
				accept = 1;
			} else if (key == KeyEvent.VK_B) {
				currentState = STATE.PIECE;
			}
			break;
		case EXTRA:
			if (key == KeyEvent.VK_RIGHT) {
				do {
					direction = (direction + 1) % 4;
				} while (!moveList[direction] && !moveList[direction + 4]);

			} else if (key == KeyEvent.VK_LEFT) {
				do {
					direction = (direction + 3) % 4;
				} while (!moveList[direction] && !moveList[direction + 4]);
			} else if (key == KeyEvent.VK_ENTER) {
				currentState = STATE.ACCEPT;
				accept = 1;
			}
			break;

		case ACCEPT:

			if (key == KeyEvent.VK_RIGHT) {
				if (accept == 0) {
					accept = 1;
				}
			} else if (key == KeyEvent.VK_LEFT) {
				if (accept == 1) {
					accept = 0;
				}
			} else if (key == KeyEvent.VK_ENTER) {
				if (accept == 1) {
					performMove();
				} else {
					currentState = STATE.DIRECTION;
				}
			}
			break;
		default:
			break;
		}
	}

	public void keyReleased(KeyEvent e) {

	}

	private void performMove() {
		int from = space;
		int destination = 0;
		int over = 66;

		switch (direction) {
		case 0:
			if (moveList[0]) {
				destination = space + 7;
			} else {
				destination = space + 14;
				over = space + 9;
			}
			break;
		case 1:
			if (moveList[1]) {
				destination = space + 9;
			} else {
				destination = space + 18;
				over = space + 9;
			}
			break;
		case 2:
			if (moveList[2]) {
				destination = space - 9;
			} else {
				destination = space - 18;
				over = space - 9;
			}
			break;
		case 3:
			if (moveList[3]) {
				destination = space - 7;
			} else {
				destination = space - 14;
				over = space - 7;
			}
			break;
		}
		if (over == 66) {
			board.basicMove(from, destination);
			out.println(from +"."+destination + "." + over);
			out.flush();
			currentState = STATE.WAITING;
		} else {
			boolean test = board.jumpMove(from, destination, over);
			out.println(from +"."+destination + "." + over);
			out.flush();
			if(test) {
				currentState = STATE.EXTRA;
				space = destination;
				moveList = board.moveList(space, moveList);
				if (moveList[0] || moveList[4]) {
					direction = 0;
				} else if (moveList[1] || moveList[5]) {
					direction = 1;
				} else if (moveList[2] || moveList[6]) {
					direction = 2;
				} else if (moveList[3] || moveList[7]) {
					direction = 3;
				}
			} else {
				currentState = STATE.WAITING;
			}
		}
	}

	public static void main(String args[]) {
		CGame game = new CGame();
		if (args.length > 0) {
			game.setHost(false);
			// connect to host through args[1]?
			try {
				Socket socket = new Socket(InetAddress.getByName(args[0]), 4444);
				game.setSocket(socket);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			game.setHost(true);
			// wait for connection using listen command
			try {
				ServerSocket serverSocket = new ServerSocket(4444);
				Socket socket = serverSocket.accept();
				game.setSocket(socket);
				serverSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		game.setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		game.setMaximumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		game.setMinimumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));

		JFrame frame = new JFrame(game.TITLE);
		frame.add(game);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		game.start();

	}

}
